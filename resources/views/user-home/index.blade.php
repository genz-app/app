<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jogja Art & Antique</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-5.0.2-dist/css/bootstrap.min.css') }}">
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous"> --}}
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"><img style="width: 225px;" src="{{ asset('image/logo jogja.png') }}"
                    alt=""></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Dropdown
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
                <div class="d-flex">
                    <div class="btn btn-md" style="background-color: #926727;"><a href="#"
                            style="color: white;
                            text-decoration: none;">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('image/1.jpg') }}" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{ asset('image/2.jpg') }}" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{ asset('image/3.jpg') }}" class="d-block w-100" alt="...">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <div class="container mt-5">
        <div class="row p-2">
            <div class="col-6">
                <h1 style="color: #926727;">Our Collections</h1>
            </div>
            <div class="col-6 pt-2">
                <div class="btn btn-primary float-end" style="background-color: #926727; border: none;">See All
                    Collections</div>
            </div>
        </div>
        <div class="card-group">
            <div class="card p-2">
                <img src="{{ asset('image/produk-1.jpg') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title " style="color: #926727;">Modular Chaise Lounge Right-Arm</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                        additional
                        content. This content is a little bit longer.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
            <div class="card p-2">
                <img src="{{ asset('image/produk-2.jpg') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title " style="color: #926727;">Rectangular Coffee Table Low</h5>
                    <p class="card-text">This card has supporting text below as a natural lead-in to additional
                        content.
                    </p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
            <div class="card p-2">
                <img src="{{ asset('image/produk-3.jpg') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title " style="color: #926727;">Javea Sofa 2 Seaters</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to
                        additional
                        content. This card has even longer content than the first to show that equal height action.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col-6">
                <img width="100%" src="{{ asset('image/about.jpg') }}" alt="">
            </div>
            <div class="col-6">
                <h1 style="color: #926727;">About Jogja Art & Antique Furniture</h1>
                <p>OTAZEN, founded in the nineties, has become a benchmark in the furniture sector, manufacturing first
                    quality furniture for companies from different countries reaching five continents.

                    OTAZEN, always led by its founding team, has experienced a progressive increase in activities,
                    positioning itself as a leading furniture production company for the different hotel, domestic and
                    contract sectors.</p>
            </div>
        </div>

    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
</body>

</html>
