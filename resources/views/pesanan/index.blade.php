@extends('layout/template')

@section('judul_halaman', 'Pesanan')

@section('konten')

    <body>
        <div class="container p-4">
            <div class="card card-body p-4" style="font-size: 10pt;">
                <div class="pb-3">
                    <a href='pesanan/create' class="btn btn-primary tombol-tambah">+ Tambah Data</a>
                </div>
                <table class="table table-bordered table-responsive m-0" id="tbl-pesanan">
                    <thead>
                        <tr>
                            <th class="col-1">ID</th>
                            <th class="col-2">Item</th>
                            <th class="col-2">Waktu</th>
                            <th class="col-1">Quantity</th>
                            <th class="col-2">Total Bayar</th>
                            <th class="col-1">Kembalian</th>
                            <th class="col-2">Action</th>
                        </tr>
                    </thead>
                </table>
                <!-- /.table-responsive -->
            </div>
        </div>
        @include('pesanan.script')
    </body>
@endsection
