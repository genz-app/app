@extends('layout/template')

@section('judul_halaman', 'Pesanan')

@section('konten')

    <div class="container p-4">
        <div class="card shadow">
            <div class="card-header">
                <h4>Tambah Pesanan</h4>
            </div>
            <div class="card-body">
                <form action="" method="POST">

                    <div class="mb-3 row">
                        <label for="item" class="col-sm-2 col-form-label">Item</label>

                        <div class="col-sm-10">
                            <select name="item" id="item" class="form-control" id="item">
                                @foreach ($data_barang as $data)
                                    <option value="{{ $data->id_barang }}">{{ $data->nama_barang }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="email" class="col-sm-2 col-form-label">Waktu</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name='waktu' id="waktu">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="email" class="col-sm-2 col-form-label">Quantity</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name='quantity' id="quantity">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="email" class="col-sm-2 col-form-label">Total Bayar</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name='total_bayar' id="total_bayar">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="email" class="col-sm-2 col-form-label">Kembalian</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name='kembalian' id="kembalian">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <input type="submit" value="Tambah" class="btn btn-primary tombol-tambah">
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    @include('pesanan.script')


@endsection
