<link rel="stylesheet" href="{{ asset('plugins/datatables/datatables.min.css') }}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#tbl-pesanan').DataTable({
            "processing": true,
            "serverside": true,
            "ajax": "{{ url('pesananAjax') }}",
            "columns": [

                {
                    data: 'id_detail',
                    name: 'iddetail'
                }, {
                    data: 'nama_barang',
                    name: 'namabarang'
                },
                {
                    data: 'tgl_transaksi',
                    name: 'tgltransaksi'
                },
                {
                    data: 'quantity',
                    name: 'Quantity'
                },
                {
                    data: 'total_bayar',
                    name: 'totalbayar'
                }, {
                    data: 'kembalian',
                    name: 'Kembalian'
                }, {
                    data: 'action',
                    name: 'Action'
                }
            ]
        });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.tombol-tambah').click(function() {
        $.ajax({
            url: 'pesananAjax',
            type: 'POST',
            data: {
                item: $('#item').val(),
                waktu: $('#waktu').val(),
                quantity: $('#quantity').val(),
                total_bayar: $('#total_bayar').val(),
                kembalian: $('#kembalian').val()
            },
            success: function(response) {
                console.log(response);
            }

        })
    })
</script>
