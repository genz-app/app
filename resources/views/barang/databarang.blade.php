@extends('layout/template')

@section('judul_halaman', 'Data Barang')

@section('konten')
    <div class="container p-4">
        <div class="card">
            <div class="card-header border-transparent">
                <h3 class="card-title">Data Barang</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <a href="javascript:void(0)" class="btn btn-success mb-2" id="btn-create-post">TAMBAH</a>
                <div class="table-responsive">
                    <table class="table m-0" id="tbl-diskon">
                        <thead>
                            <tr>
                                <th>ID Barang</th>
                                <th>ID Kategori</th>
                                <th>Nama Barang</th>
                                <th>Harga Barang</th>
                                <th>Gambar</th>
                                <th>Stok</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($barang as $data)
                                <tr id="index_{{ $data->id_barang }}">
                                    <td>{{ $data->id_barang }}</td>
                                    <td>{{ $data->nama_kategori }}</td>
                                    <td>{{ $data->nama_barang }}</td>
                                    <td>{{ $data->harga_barang }}</td>
                                    <td>{{ $data->gambar }}</td>
                                    <td>{{ $data->stok_barang }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-sm btn-primary float-right">View All Orders</a>
            </div>
            <!-- /.card-footer -->
        </div>
    </div>

@endsection
