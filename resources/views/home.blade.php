<style>
    .info-box {
        background-color: #4C4C6D;
    }

    .info-box-text {
        font-size: 10pt;
        color: #ffffffb6;
    }

    .info-box-icon {
        color: #28E5C4;
    }

    .info-box-number {
        font-size: 16pt;
    }

    th,
    td {
        font-size: 10pt;
        vertical-align: middle;
    }
</style>

@extends('layout/template')

@section('judul_halaman', 'Beranda')

@section('konten')

    <div class="container p-4">
        {{-- card --}}
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6">
                <div class="info-box"
                    style="background-image: url('https://i.ibb.co/NyptF9V/Background.png'); color: #fff; padding: 15px 0;">
                    <span class="info-box-icon"><i class="fa-solid fa-dollar-sign"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Pendapatan bersih bulan ini</span>
                        <span class="info-box-number">
                            Rp. 6.203.000
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon "><i class="fa-solid fa-box"></i></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Data Barang</span>
                        <span class="info-box-number">49</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon"><i class="fa-solid fa-users"></i></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Pelanggan Setia</span>
                        <span class="info-box-number">2,000</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>

        {{-- tabel pesanan terbaru --}}
        <div class="card">
            <div class="card-header border-transparent">
                <h3 class="card-title">Pesanan Terbaru</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table m-0">
                        <thead>
                            <tr>
                                <th>Timestamp</th>
                                <th>Item</th>
                                <th>Qty</th>
                                <th>Customer</th>
                                <th>Total Harga</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="pages/examples/invoice.html">19/9/2023 19.00</a></td>
                                <td>Modular Ecuador Dining Table</td>
                                <td>100</td>
                                <td>Otazen Factory</td>
                                <td>IDR 29.459.000</td>
                                <td>
                                    <div class="btn btn-sm btn-primary">Detail</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">19/9/2023 19.00</a></td>
                                <td>Modular Ecuador Dining Table</td>
                                <td>100</td>
                                <td>Otazen Factory</td>
                                <td>IDR 29.459.000</td>
                                <td>
                                    <div class="btn btn-sm btn-primary">Detail</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">19/9/2023 19.00</a></td>
                                <td>Modular Ecuador Dining Table</td>
                                <td>100</td>
                                <td>Otazen Factory</td>
                                <td>IDR 29.459.000</td>
                                <td>
                                    <div class="btn btn-sm btn-primary">Detail</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">19/9/2023 19.00</a></td>
                                <td>Modular Ecuador Dining Table</td>
                                <td>100</td>
                                <td>Otazen Factory</td>
                                <td>IDR 29.459.000</td>
                                <td>
                                    <div class="btn btn-sm btn-primary">Detail</div>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="pages/examples/invoice.html">19/9/2023 19.00</a></td>
                                <td>Modular Ecuador Dining Table</td>
                                <td>100</td>
                                <td>Otazen Factory</td>
                                <td>IDR 29.459.000</td>
                                <td>
                                    <div class="btn btn-sm btn-primary">Detail</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-sm btn-primary float-right">View All Orders</a>
            </div>
            <!-- /.card-footer -->
        </div>

        {{-- grafik rekapan --}}
        <div class="row">
            <div class="col-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Line Chart</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="lineChart"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </div>

@endsection
