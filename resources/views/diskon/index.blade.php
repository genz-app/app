@extends('layout/template')

@section('judul_halaman', 'Data Diskon & Voucher')

@section('konten')
    <main class="container">

        <!-- START DATA -->
        <div class="my-3 p-3 bg-body rounded shadow-sm">
            <!-- TOMBOL TAMBAH DATA -->
            <div class="pb-3">
                <a href='' class="btn btn-primary">+ Tambah Data</a>
            </div>
            <table class="table table-striped" id="tbl-diskon">
                <thead>
                    <tr>
                        <th class="col-1">ID</th>
                        <th class="col-4">Kode Diskon</th>
                        <th class="col-4">Nominal</th>
                    </tr>
                </thead>
            </table>

        </div>
        <!-- AKHIR DATA -->
    </main>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection
