@extends('layout/template')

@section('judul_halaman', 'Laporan Transaksi')

@section('konten')
    <div class="container p-4">
        <div class="card">
            <div class="card-header border-transparent">
                <h3 class="card-title">Laporan Transaksi</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-primary">
                        Print
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table m-0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Id Transaksi</th>
                                <th>Nama Barang</th>
                                <th>Tanggal Transakski</th>
                                <th>Quantity</th>
                                <th>Total Bayar</th>
                                <th>Kembalian</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach ($detail as $list)
                                <tr>
                                    <td>{{ $loop->iteration}}</td>
                                    <td>{{ $list->id_transaksi}}</td>
                                    <td>{{ $list->nama_barang}}</td>
                                    <td>{{ $list->tgl_transaksi}}</td>
                                    <td>{{ $list->quantity}}</td>
                                    <td>{{ $list->total_bayar}}</td>
                                    <td>{{ $list->kembalian}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-sm btn-primary float-right">View All Orders</a>
            </div>
            <!-- /.card-footer -->
        </div>
    </div>

@endsection