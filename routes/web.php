<?php

use App\Http\Controllers\barangController;
use App\Http\Controllers\pesananController;
use App\Http\Controllers\detailtransaksiController;
use App\Http\Controllers\diskonAjaxController;
use App\Http\Controllers\diskonController;
use App\Http\Controllers\homeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('barang', [barangController::class, 'index']);

Route::get('detailtransaksi', [detailTransaksiController::class, 'index']);

Route::get('home', [homeController::class, 'index']);
Route::get('/diskon', function () {
    return view('diskon.index');
});
Route::resource('diskonAjax', diskonAjaxController::class);


Route::resource('pesananAjax', pesananController::class);
Route::get('/pesanan', function () {
    return view('pesanan.index');
});

Route::get('/pesanan/create', [pesananController::class, 'create']);
