<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->id('id_transaksi')->unique();
            $table->bigInteger('id_user')->unsigned();
            $table->bigInteger('id_barang')->unsigned();
            $table->bigInteger('id_diskon')->unsigned();
        });
        Schema::table('transaksis', function (Blueprint $table) {        
            $table->foreign('id_user')->references('id_user')->on('user_auths')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_barang')->references('id_barang')->on('barangs')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_diskon')->references('id_diskon')->on('diskons')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaksi');
    }
};
