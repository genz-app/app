<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_auths', function (Blueprint $table) {
            $table->id('id_user')->unique();
            // $table->foreignId('id_profile')->unsigned();
            $table->enum('role', ['admin', 'customer']);
            $table->string('password');
            $table->string('email');
        });

        // Schema::table('user_auths', function (Blueprint $table) {        
        //     $table->foreign('id_profile')->references('id_profile')->on('user_profiles')->onDelete('cascade')->onUpdate('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user__models');
    }
};
