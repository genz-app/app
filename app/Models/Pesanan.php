<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    use HasFactory;

    public $table = "detail_transaksis";

    protected $fillable = ['id_detail', 'id_barang', 'tgl_transaksi', 'quantity', 'total_bayar', 'kembalian'];
}
