<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;

    protected $table = 'barangs';
    protected $primaryKey = 'id_barang';

    protected $fillable = ['id_barang', 'id_kategori', 'nama_barang', 'harga_barang', 'gambar', 'stok_barang'];

    public  $timestamps = false;
}
