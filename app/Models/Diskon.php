<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diskon extends Model
{
    use HasFactory;

    public $table = 'diskons';
    // protected $primaryKey = 'id_diskon';

    protected $fillable = ['id_diskon', 'kode_diskon', 'nominal'];

    // public  $timestamps = false;
}
