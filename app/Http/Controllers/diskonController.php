<?php

namespace App\Http\Controllers;

use App\Models\Diskon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;

class diskonController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Diskon::all();
        return DataTables::of($data)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('diskon.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_diskon'     => 'required',
            'kode_diskon'     => 'required',
            'nominal'   => 'required'
        ]);

        //create post
        Diskon::create([
            'id_diskon'     => $request->id_diskon,
            'kode_diskon'     => $request->kode_diskon,
            'nominal'   => $request->nominal
        ]);

        //redirect to index
        return redirect()->route('diskon.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
